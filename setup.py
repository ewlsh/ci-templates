#!/usr/bin/env python3

from setuptools import setup

setup(name='ci-fairy',
      version='0.1',
      description='Commandline tools for the freedesktop.org CI',
      url='http://gitlab.freedesktop.org/freedesktop/ci-templates',
      author='The fdo collective',
      author_email='check.the@git.history',
      license='MIT',
      package_dir={'': 'tools'},
      packages=[''],
      py_modules=['ci_fairy'],
      entry_points={
          'console_scripts': [
              'ci-fairy = ci_fairy:ci_fairy',
          ]
      },
      classifiers=[
          'Development Status :: 3 - Alpha',
          'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.6'
      ],
      python_requires='>=3.6',
      include_package_data=True,
      install_requires=[
          'python-gitlab',
          'boto3',
          'click',
          'colored',
          'GitPython',
          'pyyaml',
          'jinja2',
      ],
      )
